# Usage

```bash
docker run -it --rm -v $(pwd):/app kalmac/pfaedle -h
```

All files in current directory are accessible to `pfaedle`.

To know more about `pfaedle`, see the
[Github page](https://github.com/ad-freiburg/pfaedle)
