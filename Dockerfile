FROM debian:buster AS builder

WORKDIR /app

RUN apt-get update; \
  apt-get install -y g++ cmake git; \
  rm -rf /var/lib/apt/lists/*

RUN git clone --recurse-submodules https://github.com/ad-freiburg/pfaedle
RUN mkdir pfaedle/build

RUN cd pfaedle/build; \
  cmake ..; \
  make -j2; \
  pwd; \
  make install

FROM debian:buster
RUN apt-get update; \
  apt-get install -y libgomp1; \
  rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/etc/pfaedle /usr/local/etc/pfaedle
COPY --from=builder /usr/local/bin/pfaedle /usr/local/bin/pfaedle

ENTRYPOINT ["pfaedle"]
